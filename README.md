# sails-db2fori

**A Sails/Waterline ORM for DB2 for i.**

## References
- Project was originally cloned from [sails-adapter-boilerplate](https://github.com/balderdashy/sails-adapter-boilerplate).

- [Sails Custom Adapter Instructions](http://sailsjs.com/documentation/concepts/extending-sails/adapters/custom-adapters)

- The [Sails DB2 LUW adapter repo](https://github.com/mikermcneil/sails-db2).  We can use this as a reference.

- Interface will be with IBM's DB2 for i adapter, as documented [here](http://bit.ly/dw-nodejs-async).

# Please Help
See the [Issues](https://bitbucket.org/litmis/sails-db2fori/issues?status=new&status=open) area for areas we need help.


# License
MIT (see LICENSE file)